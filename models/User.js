const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	email:{
		type: String,
		required: [true, "Please input your email"]
	},

	password : {
		type : String,
		required : [true, "Password is required"]
	},

	isAdmin : {
		type : Boolean,
		default : false
	}

});

module.exports = mongoose.model("User", userSchema);