const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({

	userId: mongoose.ObjectId,

	products:[
		{details:{
			type: Object,
			required: [true, "Product information required"]
		},
		quantity:{
			type: Number,
			default: 0
		},
		subtotal:{
			type: Number,
			default: 0
		}}
	],

	totalPrice: {
		type: Number,
		default: 0
	}
})


module.exports = mongoose.model("Cart", cartSchema);