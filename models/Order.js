const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId: mongoose.ObjectId,

	products: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Product Quantity is required"]

			}
		},
		],
	

	totalAmount: {
		type: Number,
		default: null
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}


});

module.exports = mongoose.model("Order", orderSchema);