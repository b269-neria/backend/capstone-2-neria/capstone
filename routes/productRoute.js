const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for new product

router.post("/add", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for get all active product
router.get("/allActive", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Route for get all active product
router.get("/all", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	productController.getAllProducts(isAdmin).then(resultFromController => res.send(resultFromController));
});

// Route for specific product
router.get("/:productId" , (req, res)=>{
	productController.getSpecificProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Update Product
router.put("/:productId/update", auth.verify , (req, res)=>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(data,req.params).then(resultFromController => res.send(resultFromController));
});

// Archive Product
router.patch("/:productId/archive",auth.verify, (req,res)=>{
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(isAdmin, req.params).then(resultFromController => res.send(resultFromController));
});

// Activate Product
router.patch("/:productId/activate",auth.verify, (req,res)=>{
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.activateProduct(isAdmin, req.params).then(resultFromController => res.send(resultFromController));
});




module.exports = router;
