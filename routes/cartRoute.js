const express = require("express");
const router = express.Router();
const auth = require("../auth");
const cartController = require("../controllers/cartController");


// Add to Cart
router.post("/add", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		userId: auth.decode(req.headers.authorization).id,
	}
	cartController.addToCart(data).then(resultFromController => res.send(resultFromController));
});


// Edit quantity
router.post("/edit", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		userId: auth.decode(req.headers.authorization).id,
	}
	cartController.changeQuantity(data).then(resultFromController => res.send(resultFromController));
});

// Remove Item from cart
router.post("/remove", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		userId: auth.decode(req.headers.authorization).id,
	}
	cartController.removeFromCart(data).then(resultFromController => res.send(resultFromController));
});

router.get("/view", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
	}
	cartController.viewCart(data).then(resultFromController => res.send(resultFromController));
});
module.exports = router;