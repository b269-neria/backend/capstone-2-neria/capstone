const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController");


// Route for Non-admin new Order
router.post("/new", auth.verify, (req, res) => {
	const data = {
		order: req.body,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
});


// Retrieve Auth User Orders
router.get("/viewOrders", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
	}
	orderController.getUserOrders(data).then(resultFromController => res.send(resultFromController));
});

// Retrieve all Orders
router.get("/all", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}
	orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;