const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Register route
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Login Route
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User details
router.get("/:userId/details", (req,res)=> {
userController.getSpecificUser(req.params).then(resultFromController => res.send(resultFromController));
});


// Promote Non-admin
router.patch("/promote", auth.verify, (req,res)=>{

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	userController.promoteUser(isAdmin, req.body).then(resultFromController => res.send(resultFromController));
});

// Demote Admin
router.patch("/demote", auth.verify, (req,res)=>{

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	userController.demoteUser(isAdmin, req.body).then(resultFromController => res.send(resultFromController));
});


// Check Email
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Get details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

module.exports = router;