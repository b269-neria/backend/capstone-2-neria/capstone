const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const cartRoute = require("./routes/cartRoute");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://mailkneria:admin123@zuitt-bootcamp.dw80oi3.mongodb.net/capstoneAPIv2?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);
app.use("/cart", cartRoute);

mongoose.connection.once("open",() => console.log('We are now connected to the cloud database'));

app.listen(process.env.PORT || 4012,()=> console.log(`We are now connected to the ${process.env.PORT || 4012} port`));
