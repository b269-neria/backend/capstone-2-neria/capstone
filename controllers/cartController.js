const Cart = require("../models/Cart");
const Product = require("../models/Product");

// Adding to Cart
module.exports.addToCart = async (data) =>{

	let product = await Product.findById(data.product.productId).then(product=> {
		return product;
	});
	let existingCart = await Cart.findOne({userId: data.userId}).then(cart => {
		return cart;
	});

	if (existingCart == null) {
		let newCart = await new Cart ({
			userId: data.userId,
			products: [{
				details: product,
				quantity: data.product.quantity,
				subtotal: data.product.quantity*product.price
			}],
			totalPrice: data.product.quantity*product.price
		})

		return await newCart.save().then((cart,error)=> {
		if (error){
			return false;
		} else {
			return "Succesfully Added to Cart";
		};
	});
	} else {
		let addedPrice = data.product.quantity*product.price;

		existingCart.products.push({
				details: product,
				quantity: data.product.quantity,
				subtotal: addedPrice
		});

		let sum = 0;

		existingCart.products.forEach(pcs => {
			sum += pcs.subtotal
		})

		existingCart.totalPrice = sum;

		return await existingCart.save().then((cart,error)=> {
		if (error){
			return false;
		} else {
			return "Succesfully Added to Cart";
		};
	});
};
};


// Adjusting Quantity
module.exports.changeQuantity = async (data) => {
		let existingCart = await Cart.findOne({userId: data.userId}).then(cart => {
		return cart;
	});

	if (existingCart == null) {
		return "You don't have an existing Cart yet."
	} else {
		let found = existingCart.products.findIndex(product => product.details._id == data.product.productId);
		let newPrice = existingCart.products[found].details.price*data.product.quantity;
		existingCart.products[found].quantity = data.product.quantity;
		existingCart.products[found].subtotal = newPrice;

		let sum = 0;

		existingCart.products.forEach(pcs => {
			sum += pcs.subtotal
		})

		existingCart.totalPrice = sum;

		return await existingCart.save().then((cart,error)=> {
		if (error){
			return false;
		} else {
			return "Succesfully edited the product in the cart";
		};
	});
};
};

// Removing an item from cart
module.exports.removeFromCart = async (data) => {
		let existingCart = await Cart.findOne({userId: data.userId}).then(cart => {
		return cart;
	});

	if (existingCart == null) {
		return "You don't have an existing Cart yet."
	} else {
		let found = existingCart.products.findIndex(product => product.details._id == data.product.productId);
		existingCart.products.splice(found,1);


		let sum = 0;

		existingCart.products.forEach(pcs => {
			sum += pcs.subtotal
		})

		existingCart.totalPrice = sum;

		return await existingCart.save().then((cart,error)=> {
		if (error){
			return false;
		} else {
			return "Succesfully removed item from cart";
		};
	});
};
};

// Display Cart

module.exports.viewCart = async (data) =>{

	let existingCart = await Cart.findOne({userId: data.userId}).then(cart => {
		return cart;
	});

	if (existingCart == null) {
		return "You don't have an existing Cart yet."
	} else {return existingCart};
};