const Order = require("../models/Order");
const Product = require("../models/Product");

// Create new order for non-admin


module.exports.createOrder = async (data) => {

	if (data.isAdmin !== true){
	let total = await Product.findById(data.order.productId).then(product=> {
		return product.price*data.order.quantity
	});

	let newOrder = await new Order({
		userId: data.userId,
		products: [{productId: data.order.productId, quantity: data.order.quantity}],
		totalAmount: total
	});


	return await newOrder.save().then((order,error)=> {
		if (error){
			return false;
		} else {
			return true;
		};
	});

	} else {
		return false;
	}
};


// Retrieve Orders of Specific Auth User

module.exports.getUserOrders = (data) => {
	return Order.find({userId: data.userId}).then(result => {
		return result;
	});
};


// Retrieve all Orders (admin)

module.exports.getAllOrders = (data) => {
	return Order.find({}).then(result => {
		if(data.isAdmin){
		return result;}
		
		else {
			return "You don't have admin privileges"
		}
	});
};
