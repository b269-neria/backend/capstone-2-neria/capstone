	const Product = require("../models/Product");

// Create new product
module.exports.addProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product ({
			productName : data.product.productName,
			description : data.product.description,
			price : data.product.price
		});
		return newProduct.save().then((product, error) =>{
			if (error){
				return false;
			} else {
				return true;
			};
		});
	};
	let message = Promise.resolve('You do not have admin privileges');
	return message.then((value) =>{
		return {value};
	});
};

// Retrieve all Products
module.exports.getAllProducts = (isAdmin) => {
	return Product.find({}).then(result => {
		if(isAdmin){
		return result;}
		
		else {
			return false;
		}
	});
};

// Retrieve all active products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieve specific product
module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// Update product

module.exports.updateProduct = (data,reqParams) => {
	if (data.isAdmin) {
	let updatedProduct = {
		productName: data.product.productName,
		description: data.product.description,
 		price: data.product.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((game, error)=> {
		if (error){
			return false
		} else {
			return true
		};
	});
};
	let message = Promise.resolve('You do not have admin privileges');
	return message.then((value) =>{
		return {value};
	});
};


// Archive Product
module.exports.archiveProduct = (isAdmin, reqParams) => {
	if (isAdmin) {
	let archivedProduct = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error)=> {
		if (error){
			return false
		} else {
			return true
		};
	});
};
	let message = Promise.resolve('You do not have admin privileges');
	return message.then((value) =>{
		return {value};
	});
};

// Activate Product

module.exports.activateProduct = (isAdmin, reqParams) => {
	if (isAdmin) {
	let activatedProduct = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productId, activatedProduct).then((product, error)=> {
		if (error){
			return false
		} else {
			return true
		};
	});
};
	let message = Promise.resolve('You do not have admin privileges');
	return message.then((value) =>{
		return {value};
	});
};


