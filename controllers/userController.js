const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register with email and password
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	});

	return newUser.save().then((user,error)=> {
		if (error){
			return false;
		} else {
			return true;
		};
	});
};

// Auth registered user
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}	else{
				return false
			};
		};
	});
};

// Retrieve Specific user
module.exports.getSpecificUser = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		result.password = "";
		return result;
	});
};


// Promote Non-admin

module.exports.promoteUser = (isAdmin, reqBody) => {
	if (isAdmin) {
	let promotedUser = {
		isAdmin: true
	};

	return User.findByIdAndUpdate(reqBody.userId, promotedUser).then((user, error)=> {
		if (error){
			return false
		} else {
			return "User given Admin Access"
		};
	});
};
	let message = Promise.resolve('You do not have admin privileges');
	return message.then((value) =>{
		return {value};
	});
};

// Demote Admin

module.exports.demoteUser = (isAdmin, reqBody) => {
	if (isAdmin) {
	let demotedUser = {
		isAdmin: false
	};

	return User.findByIdAndUpdate(reqBody.userId, demotedUser).then((user, error)=> {
		if (error){
			return false
		} else {
			return "User removed of Admin Access"
		};
	});
};
	let message = Promise.resolve('You do not have admin privileges');
	return message.then((value) =>{
		return {value};
	});
};

// Check if email is active
module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		};
	});
};

// Retreive user details
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = "*****"
			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};

